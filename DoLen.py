#
# (C) Do Le Quoc, 2014
#

import redis
import os
import sys
import time
import threading
import signal
import socket
import logging
import argparse
import subprocess
from BeautifulSoup import BeautifulSoup as bs
from xml.etree import ElementTree
import urllib2
import psutil

#####CONSTANTS######
TERMINATE  = False
REPORT_PERIOD = 1.0 ##sec
####################


############NETWORK STRUCTURE#######
vms = { '10.101.0.44' : 'crawler0',
        '10.101.0.45' : 'crawler1',
        '10.101.0.46' : 'crawler2',

}
####################################



#########REDIS COMMUNICATION###############
###MESSAGE STRUCTURE####
#key - cloud:vm:vd 
#score - timestamp 
#value - metric1:value1, metric2:value2, metric3:value3, ... 
def setVariable(timestamp, cloud, vm, source, values):
    key = ":".join([str(cloud), str(vm), str(source)])
    score = timestamp
    value = addToStr(addToStr('timestamp', timestamp, ':'), values)
    rServer.zadd(key, value, score)

def getKeys():
    keypattern = "*"
    response = rServer.keys(keypattern)
    return response
###########################################


#########HELPER FUNCTION###############
def addToStr(stats, value, delim = ','):
    if stats == '':
        stats = value
    else:
        stats = delim.join([str(stats), str(value)]) 
    return stats

def print_xml(ctx, path):
    res = ctx.xpathEval(path)
    if res is None or len(res) == 0:
        value = None
    else:
        value = res[0].content
    return value

def getvms():
    vms = []
    keys = getKeys()
    for line in keys:
        elements = line.split(":")
        vms.append(elements[1])
    return vms
###########################################


#########VM DAEMON#############
#########METRICS###############
class Memory :

    def __init__(self, logger):
        self.metrics = ['MemTotal', 'MemFree', 'Buffers', 'Cached', 'SwapTotal', 'SwapFree', 'CPU']
        self.logger = logger

    def probe(self, elapsed_sec, vmip) :
        try:
            stats = ''
            fin = open('/proc/meminfo', 'r')
            for line in fin:
                elements = line.split()   
                metric = elements[0].split(':')[0]
                if metric in self.metrics:
                    stats = addToStr(stats, addToStr(metric, elements[1], ':'))
            fin.close()
            cpu = psutil.cpu_percent()
            stats = addToStr(stats, addToStr('CPU', cpu, ':'))
        except Exception, e:
            self.logger.error("Error in Memory probe method : " + str(e))

        #print stats
        return stats


class Network :

    rx_prev_byte = {}
    tx_prev_byte = {}

    def __init__(self, logger):
        self.metrics = metrics()
        self.ifaces()
        self.logger = logger

    def ifaces(self):
        f = open('/proc/net/dev', 'r')
        for line in f.readlines()[2:] :
            iface = line.split(':')[0].strip()
            self.rx_prev_byte[iface] = 0
            self.tx_prev_byte[iface] = 0
        f.close()

    def metrics(self) :
        metrics = {}
        f = open('/proc/net/dev', 'r')
        for line in f.readlines()[2:] :
            result = []
            iface = line.split(':')[0].strip()
            result.append(iface+'_rx_byte')
            result.append(iface+'_tx_byte')
            result.append(iface+'_rx_MBs')
            result.append(iface+'_tx_MBs')
            metrics[iface] = result
        f.close()
        return metrics

    def probe(self, elapsed_sec, vmip) :
        try:
            fin = open('/proc/net/dev', 'r')
            result = ''
            for line in fin.readlines()[2:] :
                line = line.strip()
                
                (iface, vals) = line.split(':')
                vals = vals.split()

                rx_byte = int(vals[0])
                tx_byte = int(vals[8])

                if elapsed_sec == 0 :
                    rx_rate_mbps = 0
                    tx_rate_mbps = 0
                else :
                    rx_rate_mbps = ((rx_byte-self.rx_prev_byte[iface])/1024/1024)/elapsed_sec
                    tx_rate_mbps = ((tx_byte-self.tx_prev_byte[iface])/1024/1024)/elapsed_sec

                stats = addToStr(stats, addToStr(self.metrics[iface][0], rx_byte, ':'))
                stats = addToStr(stats, addToStr(self.metrics[iface][1], tx_byte, ':'))
                stats = addToStr(stats, addToStr(self.metrics[iface][2], rx_rate_mbps, ':'))
                stats = addToStr(stats, addToStr(self.metrics[iface][3], tx_rate_mbps, ':'))

                self.rx_prev_byte[iface] = rx_byte
                self.tx_prev_byte[iface] = tx_byte
        except Exception, e:
            self.logger.error("Error in Network probe method : " + str(e))
        return stats
        


class Metrics :

    def __init__(self, cloud,  vmip, logger):
        self.cloud = cloud
        self.vmip = vmip
        self.logger = logger
        self.metrics = ['user', 'nice', 'system', 'idle', 'iowait']
        cpu = '-u'
        ram = '-r'
        disk = '-d'
        net = '-n DEV'
        swap = '-W'
        interval  = 1
        self.sarProcess = subprocess.Popen(["sar", cpu, ram, disk, net, swap,  str(interval)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    def probe(self, elapsed_sec, vmip) :
            while True:
                line = self.sarProcess.stdout.readline()
                if 'CPU' in line:
                    elements = line.split()
                    line = self.sarProcess.stdout.readline()
                if 'mem' in line:
                    elements = line.split()
                    line = self.sarProcess.stdout.readline()
                if 'DEV' in line:
                    elements = line.split()
                    line = self.sarProcess.stdout.readline()
                if 'IFACE' in line:
                    elements = line.split()
                    line = self.sarProcess.stdout.readline()
                if 'swp' in line:
                    elements = line.split()
                    line = self.sarProcess.stdout.readline()
                    


#########LOGGING###############
def createLogger(source , vmname):
    loggername = 'DoLen_'+ str(source) + '_' + str(vmname)
    logger = logging.getLogger(loggername)
    hdlr = logging.FileHandler(loggername + '_.log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)
    return logger
###############################




############REPORT LOOP########
def record(probes, cloud, vms, source) :
    prev = 0
    prevms = vms
    while not TERMINATE :
        #print vms
        if source == 'app' and (len(vms) == 0 or len(vms)!=len(prevms)):
            prevms = vms
            vms = getvms()
            #prev = dict((key, 0) for key  in vms)
        for vm in vms:
            stats = ''
            now = time.time()
            elapsed_sec = 0 if prev == 0 else (now - prev)
            for p in probes :   
                stats = addToStr(stats, p.probe(elapsed_sec, vm))
            if source == 'app':
                break
            setVariable(now, cloud, vm, source, stats)
            prev = now
        if source == 'app':
            for vm in vms:
                setVariable(now, cloud, vm, source, stats)        
        time.sleep(REPORT_PERIOD)
###############################


############TERMINATE HANDLER########
def sigterm_handler(signum, frame) :
    print 'Terminate was called'
    print 'Signal handler called with signal', signum
    print "At ", frame.f_code.co_name, " in ", frame.f_code.co_filename, " line " , frame.f_lineno
    global TERMINATE
    TERMINATE = True
######################################


############MAIN###########
if __name__ == '__main__':
    signal.signal(signal.SIGINT, sigterm_handler)
    signal.signal(signal.SIGTERM, sigterm_handler)
    parser = argparse.ArgumentParser(description='Enter VM location data')
    parser.add_argument('--source', metavar='D', type=str, choices=['vm', 'host', 'app'],
                       help='data source(vm or host)')
    parser.add_argument('--cloud', metavar='C', type=str, default='None',
                       help='cloud identificator')
    parser.add_argument('--vms', metavar='I', type=str, nargs='+', default='None', 
                       help='VMs IP addresses')
    parser.add_argument('--redisserver', metavar='R', type=str, default='localhost', 
                       help='redis server ip')
    parser.add_argument('--redisport', metavar='P', type=int, default=6379,
                       help='redis server port')
    parser.add_argument('--actuator', action='store_true', default=False,
                       help='Work as host capacity manager')

    args = parser.parse_args()
    logger = createLogger(args.source, args.vms)
    try:
        infoStr = 'cloud:' + str(args.cloud) + ' vms:' + str(args.vms) + ' dataSource:' + str(args.source) + ' redisServer:' + str(args.redisserver) + ' redisPort:' + str(args.redisport) + ' actuator:' + str(args.actuator)  
        logger.info(infoStr)
        rServer = redis.Redis(host=args.redisserver, port=args.redisport, db=0)
        vmsList = list(args.vms)
        if args.source == 'vm':
            record([Memory(logger)], args.cloud, vmsList, args.source)
    except Exception, e:
        logger.error("Error in Main method : " + str(e))
###########################
